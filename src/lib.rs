use libc::{c_void, c_char, dlsym, RTLD_NEXT};
use std::mem::transmute;
use std::slice;
use nom::HexDisplay;

#[link_section = ".init_array"]
pub static INITIALIZE: extern "C" fn() = rust_ctor;

#[no_mangle]
pub extern "C" fn rust_ctor() {
    println!("[DETOUR] Loaded!");
}

// static bool CCrypto::GenerateRandomBlock( uint8 *pubDest, int cubDest );
#[no_mangle]
pub extern "C" fn _ZN7CCrypto19GenerateRandomBlockEPvj(dest: *mut u8, size: usize) -> bool {
    println!("[DETOUR] CALLED CCrypto::GenerateRandomBlock({:#?}, {})", dest, size);

    let mut retval = false;
    let ptr = unsafe { dlsym(RTLD_NEXT, "_ZN7CCrypto19GenerateRandomBlockEPvj\0".as_ptr() as *const c_char) };
    if !ptr.is_null() {
        let genblock_func = unsafe { transmute::<*const c_void, fn() -> bool>(ptr) };
        retval = genblock_func();
        let slicedata = unsafe { slice::from_raw_parts(dest, size) };
        println!("Generated {} bytes of random data.\n{}", size, slicedata.to_hex(16));
        println!("\treturning {}", retval);
    }
    retval
}

#[no_mangle]
pub extern "C" fn geteuid() -> u32 {
    println!("[DETOUR] CALLED geteuid!");
    let mut pid = 0;

    let ptr = unsafe { dlsym(RTLD_NEXT, "geteuid\0".as_ptr() as *const c_char) };
    if !ptr.is_null() {
        let gpid_func = unsafe { transmute::<*const c_void, fn() -> u32>(ptr) };
        pid = gpid_func();
        println!("\treturning {}", pid);
    }
    pid
}
